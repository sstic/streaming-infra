# streaming-infra

Infrastructure for the Streaming capability

# Overview

The test infrastructure (from the `Vagrantfile`) is made of:
- `transcoder`: get input stream and transcode it to 1080p, 720p and 540p streams
- `player`: web player, getting the stream from `transcoder`
- `player2`: web player, illustrating player chaining
- `collector`: monitoring, using the CollectD + InfluxDB + Grafana stack

![Overview](overview.svg)

# Roles

This repository contains several Ansible roles, used to provision the infrastructure.

- `base`: Update packages, add current hostname to `/etc/hosts`
  - (optional) `authorized_keys`: path to an `authorized_keys` to deploy on hosts
- `nginx-rtmp`: Compile and install NGINX with RTMP support
  - (optional) `http_rtmp_stat`: set to true to enable RTMP stat in Nginx
  - (optional) `ssl_fullchain` + `ssl_privkey` + `server_name`: paths to the PEM SSL full chain, the PEM private key and string of the SSL server name. If set, also deploy SSL on 443
  - `web_root_dir`: if overrided, location of the web root directory
- `player`: Add a Web player from `player/` and relay a RTMP stream
  - `player_rtmp_src`: source for the RTMP stream
  - `hls_dir`: if overrided, location of the HLS chunks
  - `player_qualities`: if overrided, array of quality to provide (`[low, mid, hi]`)
- `transcoder`: Add transcoding capabilities, based on FFMpeg (compile and install it)
  - `transcoder_qualities`: if overrided, array of quality to provide (`[low, mid, hi]`)
  - `ffmpeg_log_dir`: if overrided, directory for FFMpeg logging
- `monitored`: Add monitoring capability through CollectD
  - `collector_ip`: IP to reach for CollectD collection
- `collector`: Deploy a InfluxDB + Grafana stack and prepare it for a CollectD source

# Repository layout

- `packer`: Packer templates
- `player`: Web player
- `roles`: Ansible roles
- `scripts`: Diverse scripts
- `ssl`: SSL self-signed certificate for tests
- `README.md`: This file
- `Vagrantfile`: Vagrant template
- `dashboard.json`: Example Grafana dashboard
- `get-samples.sh`: Script to get free video samples
- `play-samples.sh`: Script to push video sample to a RTMP server
- `playbook.yml`: Ansible playbook used by Vagrant
- `sstic_offline_1080p.png`: Offline image

# Development

This has been tested on a Linux + Virtualbox environment. It should also work for others platforms.

## (optional) Create images with pre-compiled binaries

This step is optional. It could speed-up development, by compiling binaries (nginx, ffmpeg) once for all instead of doing it for each new deploy.

Using [packer](https://www.packer.io/):
```sh
cd packer/alpines
packer new transcoder.json
```

This might take a while. It will create a box for Vagrant in `packer/alpines/builds/`.

## Booting-up the infrastructure

Using [vagrant](https://www.vagrantup.com/):
```
vagrant up
```

This will boot the infrastructure and apply ansible configurations (provision).

If the optional packer step has not been done, replace:
```ruby
config.vm.box = "file://packer/alpines/builds/BOX_NAME.box"
```
With:
```ruby
config.vm.box = "generic/alpine311"
```

## Testing it

A few open samples can be downloaded through:
```sh
$ ./get-samples.sh
[+] Checking 'samples/01_llama_drama_1080p.mp4'
[+] Checking 'samples/02_gran_dillama_1080p.mp4'
[+] Checking 'samples/03_caminandes_llamigos_1080p.mp4'
...
```

Then, they can be pushed to the streaming infrastructure with:
```sh
$ ./play-samples.sh dev
Redirecting transcoder:1935 to 127.0.0.1:1935
ffmpeg ...
```

To watch the resulting stream from the `player`, use:
```sh
# Low quality
ffplay -i rtmp://IP_ADDRESS/hls/live_low
# Medium quality
ffplay -i rtmp://IP_ADDRESS/hls/live_mid
# High quality
ffplay -i rtmp://IP_ADDRESS/hls/live_hi
```

Depending on the configuration, `IP_ADDRESS` could be either: `127.0.0.1` (if the SSH tunnel is up and `transcoder` has the `player` role), or `192.168.50.11` / `192.168.50.13` (`player` addresses).

To reduce the CPU requirements, one can disable the different qualities, by overriding the `transcoder_qualities` variable:
```rb
    transcoder_qualities:
      - low
      # - mid
      # - hi
```
And re-applying the provisionning:
```sh
vagrant provision
```
