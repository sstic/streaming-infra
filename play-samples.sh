#!/bin/sh

if [ -z "$1" ]
then
	echo "$0 RTMP_address; Use 'dev' to send to Vagrant transcoder";
	exit 0;
fi

SRC=samples/
IP="$1"
ENDPOINT=camsrc/cam1
WAIT_BETWEEN_SAMPLES=30

if [ $IP = "dev" ]
then
	IP="127.0.0.1"
	echo "Redirecting transcoder:1935 to 127.0.0.1:1935";
	vagrant ssh --no-tty transcoder -- -L 1935:127.0.0.1:1935 -N &
	sleep 5;
fi

while true; do
	# credits="Blender Foundation - Big buck Bunny - CC by - www.bigbuckbunny.org"
	# ffmpeg -re -i ${SRC}Big\ Buck\ Bunny\ animation\ \(1080p\ HD\)-XSGBVzeBUbk.mp4 -fps_mode passthrough -vcodec libx264 -preset superfast -vprofile baseline -b:v 6000k -maxrate 6000k -bufsize 6000k -acodec copy -s 1920x1080 -vf "drawtext=box=1:boxcolor=black@0.3:fontsize=24:fontfile=/usr/share/fonts/truetype/ttf-dejavu/DejaVuSansCondensed-Bold.ttf:text='$credits':fontcolor=white@0.6:x=8:y=1056" -f flv rtmp://${IP}/${ENDPOINT} ;
	# sleep ${WAIT_BETWEEN_SAMPLES}

	credits="Caminandes - Llama Drama - CC by - caminandes.com"
	ffmpeg -re -i ${SRC}01_llama_drama_1080p.mp4 -fps_mode passthrough -vcodec libx264 -preset superfast -vprofile baseline -b:v 6000k -maxrate 6000k -bufsize 6000k -acodec copy -s 1920x1080 -vf "drawtext=box=1:boxcolor=black@0.3:fontsize=24:fontfile=/usr/share/fonts/truetype/ttf-dejavu/DejaVuSansCondensed-Bold.ttf:text='$credits':fontcolor=white@0.6:x=8:y=1056" -f flv rtmp://${IP}/${ENDPOINT} ;
	sleep ${WAIT_BETWEEN_SAMPLES}

	credits="Caminandes - Gran Dillama - CC by - caminandes.com"
	ffmpeg -re -i ${SRC}02_gran_dillama_1080p.mp4 -fps_mode passthrough -vcodec libx264 -preset superfast -vprofile baseline -b:v 6000k -maxrate 6000k -bufsize 6000k -acodec copy -s 1920x1080 -vf "drawtext=box=1:boxcolor=black@0.3:fontsize=24:fontfile=/usr/share/fonts/truetype/ttf-dejavu/DejaVuSansCondensed-Bold.ttf:text='$credits':fontcolor=white@0.6:x=8:y=1056" -f flv rtmp://${IP}/${ENDPOINT} ;
	sleep ${WAIT_BETWEEN_SAMPLES}

	credits="Caminandes - Llamingos - CC by - caminandes.com"
	ffmpeg -re -i ${SRC}03_caminandes_llamigos_1080p.mp4 -fps_mode passthrough -vcodec libx264 -preset superfast -vprofile baseline -b:v 6000k -maxrate 6000k -bufsize 6000k -acodec copy -s 1920x1080 -vf "drawtext=box=1:boxcolor=black@0.3:fontsize=24:fontfile=/usr/share/fonts/truetype/ttf-dejavu/DejaVuSansCondensed-Bold.ttf:text='$credits':fontcolor=white@0.6:x=8:y=1056" -f flv rtmp://${IP}/${ENDPOINT} ;
	sleep ${WAIT_BETWEEN_SAMPLES}

done
