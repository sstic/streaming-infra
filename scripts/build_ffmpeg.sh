#!/bin/sh
# Inspiration: https://github.com/alfg/docker-nginx-rtmp

FFMPEG_VERSION=7.0
PREFIX=/usr/local
MAKEFLAGS="-j4"
FFMPEG_BIN=/usr/local/bin/ffmpeg

echo "[+] Check if ffmpeg is already installed"
if test -f $FFMPEG_BIN;
then
	if $FFMPEG_BIN -v 2>&1 | grep -q $FFMPEG_VERSION; then
		echo "ffmpeg already installed, skipping..."
		exit 0
	fi
fi


echo "[+] Get dependencies packages"
apk add --update \
  build-base \
  coreutils \
  freetype-dev \
  lame-dev \
  libogg-dev \
  libass \
  libass-dev \
  libvpx-dev \
  libvorbis-dev \
  libwebp-dev \
  libtheora-dev \
  openssl-dev \
  opus-dev \
  pkgconf \
  pkgconfig \
  rtmpdump-dev \
  wget \
  x264-dev \
  x265-dev \
  yasm

echo http://dl-cdn.alpinelinux.org/alpine/edge/community >> /etc/apk/repositories
apk add --update fdk-aac-dev

echo "[+] Get FFmpeg source"
cd /tmp/ && \
  wget http://ffmpeg.org/releases/ffmpeg-${FFMPEG_VERSION}.tar.gz && \
  tar zxf ffmpeg-${FFMPEG_VERSION}.tar.gz && rm ffmpeg-${FFMPEG_VERSION}.tar.gz

echo "[+] Compile ffmpeg"
cd /tmp/ffmpeg-${FFMPEG_VERSION} && \
  ./configure \
  --prefix=${PREFIX} \
  --enable-version3 \
  --enable-gpl \
  --enable-nonfree \
  --enable-small \
  --enable-libmp3lame \
  --enable-libx264 \
  --enable-libx265 \
  --enable-libvpx \
  --enable-libtheora \
  --enable-libvorbis \
  --enable-libopus \
  --enable-libfdk-aac \
  --enable-libass \
  --enable-libwebp \
  --enable-postproc \
  --enable-libfreetype \
  --enable-openssl \
  --disable-debug \
  --disable-doc \
  --disable-ffplay \
  --extra-libs="-lpthread -lm" && \
  make && make install && cp -r ./ /var/opt && make distclean
