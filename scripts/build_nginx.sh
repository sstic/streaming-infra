#!/bin/sh
# Inspiration: https://github.com/alfg/docker-nginx-rtmp

NGINX_VERSION=1.24.0
NGINX_RTMP_VERSION=1.2.2
NGINX_BIN=/usr/local/nginx/sbin/nginx

echo "[+] Check if nginx is already installed"
if test -f "$NGINX_BIN";
then
	if $NGINX_BIN -v 2>&1 | grep -q $NGINX_VERSION; then
		echo "Nginx already installed, skipping..."
		exit 0
	fi
fi

echo "[+] Get dependencies packages"
if test -f /etc/alpine-release;
then
	apk add --update \
	    build-base \
	    ca-certificates \
	    curl \
	    gcc \
	    libc-dev \
	    libgcc \
	    linux-headers \
	    make \
	    musl-dev \
	    openssl \
	    openssl-dev \
	    pcre \
	    pcre-dev \
	    pkgconf \
	    pkgconfig \
	    zlib-dev
fi

if test -f /etc/debian_version;
then
	apt update -qqy
	apt install -qqy \
	    build-essential \
	    libpcre3 \
	    libpcre3-dev \
	    libssl-dev \
	    unzip \
	    zlib1g-dev \
	    zlib1g
fi

echo "[+] Get nginx source"
cd /tmp && \
  wget https://nginx.org/download/nginx-${NGINX_VERSION}.tar.gz && \
  tar zxf nginx-${NGINX_VERSION}.tar.gz && \
  rm nginx-${NGINX_VERSION}.tar.gz

echo "[+] Get nginx-rtmp module"
cd /tmp && \
  wget https://github.com/arut/nginx-rtmp-module/archive/v${NGINX_RTMP_VERSION}.tar.gz && \
  tar zxf v${NGINX_RTMP_VERSION}.tar.gz && rm v${NGINX_RTMP_VERSION}.tar.gz

echo "[+] Compile nginx with nginx-rtmp module"
cd /tmp/nginx-${NGINX_VERSION} && \
  ./configure \
  --prefix=/usr/local/nginx \
  --add-module=/tmp/nginx-rtmp-module-${NGINX_RTMP_VERSION} \
  --conf-path=/etc/nginx/nginx.conf \
  --with-threads \
  --with-file-aio \
  --with-http_ssl_module \
  --with-debug \
  --with-http_stub_status_module \
  --with-cc-opt="-Wimplicit-fallthrough=0" && \
  cd /tmp/nginx-${NGINX_VERSION} && make && make install && cp -R ./ /var/opt/
