#!/bin/sh

OUTPUT=samples/

if  [ ! -d $OUTPUT ]
then
	echo "$OUTPUT directory does not exist."
	exit 1;
fi

# check_dl_unzip target_name src_url
check_dl_unzip () {
	filename=$1
	target=${OUTPUT}${filename}
	src=$2

	echo "[+] Checking '${target}'"
	if [ ! -f $target ]
	then
		zipfile=$(mktemp --suffix ".zip")
		curl $src -o $zipfile;
		unzip $zipfile ${filename} -d ${OUTPUT};
		rm $zipfile;
	fi
}

check_dl_unzip "01_llama_drama_1080p.mp4" "http://www.caminandes.com/download/01_llama_drama_1080p.zip"
check_dl_unzip "02_gran_dillama_1080p.mp4" "http://www.caminandes.com/download/02_gran_dillama_1080p.zip"
check_dl_unzip "03_caminandes_llamigos_1080p.mp4" "http://www.caminandes.com/download/03_caminandes_llamigos_1080p.zip"

# Big Buck Bunny is slower to download
#
# target=${OUTPUT}big_buck_bunny_1080p_stereo.avi
# src="https://download.blender.org/peach/bigbuckbunny_movies/big_buck_bunny_1080p_stereo.avi"
# 
# echo "[+] Checking '${target}'"
# if [ ! -f $target ]
# then
# 	curl $src -o $target;
# fi
