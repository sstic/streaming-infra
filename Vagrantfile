# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|

  config.vm.define "transcoder" do |transcoder|
    # TODO mv into packer Vagrantfile
    transcoder.vm.box = "file://packer/alpines/builds/virtualbox-ffmpeg-nginx.box"
    config.ssh.shell = "/bin/sh"
    transcoder.vm.synced_folder "./", "/vagrant", disabled: true

    #transcoder.vm.box = "generic/alpine315"

    transcoder.vm.hostname = "transcoder"
    transcoder.vm.network "private_network", ip: "192.168.50.10"

    transcoder.vm.provider "virtualbox" do |v|
      v.cpus = 2
    end

    # Install Python, required for ansible
    transcoder.vm.provision "shell", inline: "apk add python3"

    # Ansible provisioning
    transcoder.vm.provision "ansible" do |ansible|
      ansible.playbook = "playbook.yml"
      ansible.extra_vars = {
        collector_ip: "192.168.50.12",
      }
    end
  end

  config.vm.define "player" do |player|
    player.vm.box = "generic/alpine315"

    player.vm.hostname = "player"
    player.vm.network "private_network", ip: "192.168.50.11"

    # Install Python, required for ansible
    player.vm.provision "shell", inline: "apk add python3"

    # Ansible provisioning
    player.vm.provision "ansible" do |ansible|
      ansible.playbook = "playbook.yml"
      ansible.extra_vars = {
        transcoder_public_ip: "192.168.50.10",
        collector_ip: "192.168.50.12",
        server_name: "streaming.local",
        ssl_fullchain: "ssl/streaming.local.crt",
        ssl_privkey: "ssl/streaming.local.key"
      }
    end
  end

  config.vm.define "player2" do |player|
    player.vm.box = "debian/bullseye64"
    player.vm.synced_folder "./", "/vagrant", disabled: true

    player.vm.hostname = "player2"
    player.vm.network "private_network", ip: "192.168.50.13"

    # Ansible provisioning
    player.vm.provision "ansible" do |ansible|
      ansible.playbook = "playbook.yml"
      ansible.extra_vars = {
        player_public_ip: "192.168.50.11",
        collector_ip: "192.168.50.12",
      }
    end
  end

  config.vm.define "collector" do |player|
    player.vm.box = "debian/bullseye64"
    player.vm.synced_folder "./", "/vagrant", disabled: true

    player.vm.hostname = "collector"
    player.vm.network "private_network", ip: "192.168.50.12"

    # Install Python, required for ansible
    player.vm.provision "shell", inline: "apt install -y python"

    # Ansible provisioning
    player.vm.provision "ansible" do |ansible|
      ansible.playbook = "playbook.yml"
    end
  end
end
